/* DROP DATABASE IF EXISTS ModernWays;
CREATE DATABASE IF NOT EXISTS ModernWays;
USE ModernWays;

CREATE TABLE Personen(
 Id INT AUTO_INCREMENT PRIMARY KEY,
 Voornaam VARCHAR(100) CHARACTER SET utf8mb4 NOT NULL,
 Familienaam VARCHAR(100) CHARACTER SET utf8mb4 NOT NULL,
 Geboortejaar SMALLINT NOT NULL
);

CREATE TABLE Films(
Id INT AUTO_INCREMENT PRIMARY KEY,
Naam VARCHAR(100) CHARACTER SET utf8mb4 NOT NULL,
Jaar SMALLINT NOT NULL
);

CREATE TABLE IMDBscores(
Films_id INT NOT NULL,
Score FLOAT(2,1) NOT NULL,
PRIMARY KEY(Films_id),
FOREIGN KEY(Films_id) REFERENCES Films(Id),
CONSTRAINT Chk_Score CHECK (Score >= 0.0 AND Score <=10)

);

CREATE TABLE Ratings(
Personen_id INT NOT NULL,
Films_id INT NOT NULL,
Rating FLOAT(2.1) NOT NULL,
PRIMARY KEY(Personen_id,Films_id),
FOREIGN KEY(Personen_id) REFERENCES Personen(Id),
FOREIGN KEY(Films_id) REFERENCES Films(Id),
CONSTRAINT Chk_Rating CHECK (Rating >= 0.0 AND Rating <=10)
);

INSERT INTO Personen (Id, Voornaam, Familienaam, Geboortejaar)
VALUES ('1', 'Jarno','Vandenbloock', '1999'),
	   ('2', 'Joeri','Vandenbloock', '1971'),
       ('3', 'Sigrid','Van Moer', '1971'),
       ('4', 'Rebecca','Jean', '2000'),
       ('5', 'Hilde','Tas', '1974'),
       ('6', 'Danny','Vermeulen', '1965'),
       ('7', 'Lionel','Der Boven', '1995'),
       ('8', 'Bram','Vos', '1999'),
       ('9', 'Jarno','Lapere', '1998'),
       ('10', 'Torken','De Coninck', '1999');
       
INSERT INTO films (Id, Naam, Jaar )
VALUES ('1', 'Saving Private Ryan', '1998'),
	   ('2', 'Creed', '2015'),
       ('3', 'Gladiator', '2000'),
       ('4', 'Bohemian Rhapsody', '2018'),
       ('5', 'The Wolf of Wall Street', '2013'),
       ('6', 'Grease', '1978'),
       ('7', 'Rocketman', '2019'),
       ('8', 'Het tweede gelaat', '2017'),
       ('9', 'Tiatnic', '1997'),
       ('10', 'Magic Mike', '2012'),
       ('11', 'The sound of music', '1965'),
       ('12', 'Fifty shades of grey', '2015'),
       ('13', 'We were soldiers', '2002'),
       ('14', 'Joker', '2019'),
       ('15', 'Extraction', '2020'),
       ('16', 'Iron Man', '2008'),
       ('17', 'Interstellar', '2014'),
       ('18', 'Forrest Gump', '1994'),
       ('19', 'Star Wars Episode 3', '2005'),
       ('20', 'Inception', '2010'),
       ('21', 'Lucy', '2014'),
       ('22', 'The Conjuring', '2013'),
       ('23', 'The Matrix', '1999'),
       ('24', 'The Avengers', '2010'),
       ('25', 'Top Gun', '1986');
       
INSERT INTO IMDBscores (Films_id, score)
VALUE ('1', '8.6'),
	  ('2', '7.6'),
      ('3', '8.5'),
      ('4', '8'),
      ('5', '8.2'),
      ('6', '7.2'),
      ('7', '7.3'),
      ('8', '6.4'),
      ('9', '7.8'),
      ('10', '6.1'),
      ('11', '8'),
      ('12', '4.1'),
      ('13', '7.2'),
      ('14', '8.5'),
      ('15', '6.8'),
      ('16', '7.9'),
      ('17', '8.6'),
      ('18', '8.8'),
      ('19', '7.5'),
      ('20', '8.8'),
      ('21', '6.4'),
      ('22', '7.5'),
      ('23', '8.7'),
      ('24', '8'),
      ('25', '6.9');
      
INSERT INTO ratings (Personen_id, Films_id, Rating)
VALUES ('1','1','8'),
       ('1','2','5'),
       ('1','3','6'),
       ('2','4','8'),
       ('2','5','7'),
       ('2','6','6'),
       ('3','7','9'),
       ('3','8','9'),
       ('3','9','8'),
       ('4','10','6'),
       ('4','11','7'),
       ('4','12','5'),
       ('5','13','8'),
       ('5','14','7'),
       ('5','25','9'),
       ('6','16','8'),
       ('6','17','7'),
       ('6','18','7'),
       ('7','19','6'),
       ('7','20','8'),
       ('7','21','9'),
       ('8','21','9'),
       ('8','22','8'),
       ('8','23','8'),
       ('9','24','9'),
       ('9','22','9'),
       ('9','14','7'),
       ('10','5','8'),
       ('10','6','5'),
       ('10','21','9');
       
       */

       
/*-- Oefening 1
 USE modernways;
 Select AVG(Geboortejaar)
 From Personen;
*/
/*-- oefening 2
USE modernways;
select Familienaam, COUNT(Familienaam)
from personen 
group by familienaam;
*/
/*-- oefening 3
USE modernways;
SELECT Familienaam, COUNT(Familienaam) AS "Aantal familieleden", AVG(YEAR(current_date()) - Geboortejaar) AS "Gemiddelde leeftijd"
from Personen 
group by Familienaam;
*/
/* -- oefening 4 
USE modernways;
SELECT * 
FROM films JOIN imdbscores ON films.Id = imdbscores.Films_id;
*/

/*  -- oefening 5
USE modernways;
SELECT  r.Personen_id, r.Films_id, r.Rating, f.Id, f.Naam, f.Jaar
FROM ratings r inner join Films f ON r.Films_id  = f.id
ORDER BY Personen_id;
*/

	


/*-- oefening 6
USE modernways;
SELECT r.Personen_id, r.Films_id, r.Rating, f.id, f.Naam, f.Jaar,p.Id, p.Voornaam, p.Familienaam, p.Geboortejaar
FROM personen p INNER JOIN ratings r ON p.Id = r.Personen_id INNER JOIN films f ON r.Films_id = f.Id;

*/

			

/*-- oefening 7
USE modernways;
SELECT r.Personen_id AS Personen_ID, f.id AS Film_id, r.Rating, r.Films_id AS Id, f.Naam, F.Jaar,i.Films_id, i.Score, f.Id, p.Voornaam, p.Familienaam, p.Geboortejaar
FROM Personen p INNER JOIN Ratings r ON P.Id = R.Personen_ID 
INNER JOIN Films f ON R.Films_Id = F.Id 
INNER JOIN IMDBScores I ON I.Films_Id = F.Id;
*/


/*-- oefening 8
USE modernways;
SELECT f.Naam AS Film, CONCAT(p.Voornaam,'', p.Familienaam) AS Recensent, r.Rating, i.Score AS IMDB_Rating
FROM personen p INNER JOIN ratings r ON p.id = r.Personen_id INNER JOIN films f ON r.Films_id = f.id INNER JOIN imdbscores i ON r.Films_id = i.Films_id;
*/


-- oefening 9
USE modernways;
SELECT CONCAT(p.Voornaam,' ', p.Familienaam) AS Recensent, avg(r.Rating) AS 'Gemiddelde rating', i.Score AS 'IMDB Rating', avg(abs(r.Rating - i.Score)/i.score*100) AS 'Afwijking'
FROM personen p INNER JOIN ratings r ON P.Id = r.Personen_id INNER JOIN imdbscores i ON r.Films_id = i.Films_id
GROUP BY Voornaam, Familienaam;


/*
-- oefening 10
USE modernways;
SELECT f.Naam AS Film, count(r.Films_id) AS "Aantal keer beoordeeld", i.Score AS 'IMDB Rating'
FROM ratings r  INNER JOIN films f ON r.Films_id = f.Id
                INNER JOIN imdbscores i on r.Films_id = i.Films_id
GROUP BY f.Naam
ORDER BY count(r.Films_id) ASC, i.Score DESC;

*/








