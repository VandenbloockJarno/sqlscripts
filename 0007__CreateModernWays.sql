create database ModernWays;
CREATE DATABASE IF NOT EXISTS ModernWays;
Use ModernWays;

CREATE TABLE Boeken(
    Voornaam VARCHAR(50) CHAR SET utf8mb4,
    Familienaam VARCHAR(80) CHAR SET utf8mb4,
    Titel VARCHAR(255) CHAR SET utf8mb4,
    Stad VARCHAR(50) CHAR SET utf8mb4,
    -- alleen het jaartal, geen datetime
    -- omdat de kleinste datum daarin 1753 is
    -- varchar omdat we ook jaartallen kleiner dan 1000 hebben
    Verschijningsjaar VARCHAR(4),
    Uitgeverij VARCHAR(80) CHAR SET utf8mb4,
    Herdruk VARCHAR(4),
    Commentaar VARCHAR(1000)
);